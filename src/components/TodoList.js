import React, { useState } from 'react';
import styled from 'styled-components';
import { css } from 'styled-components'
import { SortAZ, SortZA } from '@styled-icons/boxicons-regular';

const ListContainer = styled.div`
  display: flex;
  width: 90%;
  justify-content: space-between;

  @media only screen and (min-width: 768px) {
    width: 50%;
  }
`

const List = styled.ol`
  color: #777;
  align-items: flex-start;

  li {
    margin: 4px;
    word-break: break-word;
  }
`

const SortIcon = css`
  margin: 16px;
  padding: 4px;
  border-radius: 32px;
  min-width: 28px;
  background-color: #4A7C59;
  color: white;

  &:hover {
    cursor: pointer;
    background-color: #73AB84;
  }
` 

const AscendSortIcon = styled(SortAZ)`
   ${SortIcon}
`

const DescendSortIcon = styled(SortZA)`
  ${SortIcon}
`

export default function TodoList(props) {
  const [ascendSort, setAscendSort] = useState(true);

  function sortList() {
    props.sort(ascendSort);
    setAscendSort(!ascendSort);
  }

  const renderTodoList = props.todoItems.map((item, index) => {
    return <li key={index}>{item}</li>
  });

  const renderConditionalIcon = () => {
    if (ascendSort) {
      return <AscendSortIcon size="28" onClick={sortList} />
    } else {
      return <DescendSortIcon size="28" onClick={sortList} />
    }
  }

  return (
    <ListContainer>
      <List>
        {renderTodoList}
      </List>
      {props.todoItems.length > 0 ? renderConditionalIcon() : null}
    </ListContainer>
  );
}