import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import TodoForm from './components/TodoForm';
import TodoList from './components/TodoList';

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #f4f5f5;
  min-height: 100vh;
`

function Todo() {
  const [todoItems, setTodoItems] = useState([]);

  useEffect(() => {
    function loadTodoItems() {
      const todoList = JSON.parse(localStorage.getItem('todolist'));
      if (todoList) {
        setTodoItems(todoList);
      }
    }

    loadTodoItems();
  }, []);

  function updateTodoList(item) {
    const todoList = todoItems.slice();
    todoList.push(item.trim());
    localStorage.setItem('todolist', JSON.stringify(todoList));
    setTodoItems(todoList);
  }

  function sortTodoList(ascendSort) {
    const todoList = todoItems.slice();
    if (ascendSort) {
      todoList.sort();
    } else {
      todoList.reverse();
    }
    setTodoItems(todoList);
  }

  return (
    <MainContainer>
      <h1>Lista de Tarefas</h1>
      <TodoForm updateTodoList={updateTodoList}/>
      <TodoList todoItems={todoItems} sort={sortTodoList}/>
    </MainContainer>
  );
}

export default Todo;
