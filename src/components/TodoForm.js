import React from 'react';
import styled from 'styled-components';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 90%;
  @media only screen and (min-width: 768px) {
    width: 50%;
  }
`

const Input = styled.input`
  margin: 8px;
  padding: 8px;
  border-radius: 8px;
  border: 1px solid #DBDBDB;
  cursor: text;
  font-size: 14px;
  background: #FAFAFA;
  color: #777;

  &:focus {
    outline: none;
  }
`

const Error = styled.span`
  color: red;
  margin-left: 12px;
  font-size: 15px;
`

const Button = styled.button`
  margin: 4px;
  padding: 8px 16px;
  border-radius: 3px;
  background-color: #4A7C59;
  color: white;
  cursor: pointer;
  font-size: 16px;
  border: none;
  user-select: none;
  --webkit-user-select: none;

  &:hover {
    background-color: #73AB84;
  }
`

export default function TodoForm(props) {
  const formik = useFormik({
    initialValues: {
      item: '',
    },
    validationSchema: Yup.object({
      item: Yup.string()
        .min(5, 'É necessário no mínimo 5 caracteres')
        .required('Este campo não pode ser vazio')
    }),
    onSubmit: (values, { resetForm }) => {
      props.updateTodoList(values.item);
      resetForm({ values: ''});
    }
  });

  return (
    <Form onSubmit={formik.handleSubmit}>
      <Input id="item" type="text" {...formik.getFieldProps('item')}/>
      {formik.touched.item && formik.errors.item ? <Error>(*) {formik.errors.item}</Error> : null}
      <Button type="submit">Inserir</Button>
    </Form>
  );
}