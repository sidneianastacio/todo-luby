## Informações

TodoList simples criado com [Create React App](https://github.com/facebook/create-react-app) para avaliação da empresa Luby como parte do processo seletivo.

## Características

- Responsivo
- Utiliza [Styled Components](https://styled-components.com/)
- Utiliza [Formik](https://formik.org/)
- Utiliza [Yup](https://github.com/jquense/yup)

## Como rodar o projeto

1. Instale as dependências através do `npm install`.
2. Execute `npm start` dentro do diretório do projeto.
3. Abra [http://localhost:3000](http://localhost:3000) para acessar através do navegador.